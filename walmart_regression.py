import numpy
import pandas as pd
import statsmodels.formula.api as smf
from sklearn.cross_validation import train_test_split
from sklearn.model_selection import KFold
from collections import defaultdict
from statistics import stdev

def buildFormula(keys):
    formula = 'total_sales ~ 1'
    for k in keys:
        formula += ' + ' + str(k)
    return formula


finalResult = {}

# QUERY USER FOR PRODUCT AND STORE ID
product_key = input('Product Key: ')
print

# READ DATA
train = pd.read_csv('train.csv', header=0)
all_clusters = pd.read_csv('clusters.csv', header=0)


# GET CLUSTER
cluster_id = all_clusters[all_clusters.product_key == product_key]['cluster'].iloc[0]
if cluster_id == 'None':
    print 'No valid cluster'
    exit()

# GET DATA FOR SELECTED CLUSTER
clusters = all_clusters[all_clusters.cluster == cluster_id]

otherProductsInCluster = []
for index, row in clusters.iterrows():
    if row['product_key'] != product_key:
        otherProductsInCluster.append(row['product_key'])

joined = pd.merge(clusters, train, how='inner', on='product_key')

# FOR EACH PRODUCT IN THE CLUSTER, RUN REGRESSION
for other_product_key in otherProductsInCluster:
    finalResultIndexes = []
    finalResultValues = []
    for store_id in xrange(100,145):
        try:
            print 'Store:', store_id

            # LIMIT TO DATA FOR SELECTED STORE
            store = joined[joined.store_id == store_id]


            # SPLIT DATA INTO TARGET PRODUCT AND OTHER PRODUCT IN CLUSTER
            otherProduct = store[(store.product_key == product_key)]
            targetProduct = store[(store.available_day == 1) & (store.product_key == other_product_key)]

            # INITIALIZE OBJECTS FOR ADDING COLUMNS
            data = defaultdict(lambda: defaultdict(lambda: 0))
            count = len(otherProduct.drop_duplicates(['product_key']).index)

            if count < 1:
                continue


            # BUILD LIST OF OTHER PRODUCT KEYS IN CLUSTER
            keys = []
            for i in xrange(count):
                key = otherProduct.drop_duplicates(['product_key'])['product_key'].iloc[i]
                keys.append('_' + str(key))


            # BUILD AVAILABLE DAY COLUMNS
            for index, row in otherProduct.iterrows():
                data['_' + str(row['product_key'])][row['calendar_date']] = int(row['available_day'])

            otherAvailable = pd.DataFrame(data)
            otherAvailable = otherAvailable.dropna() # Drop data with NaN

            # ADD AVAILABLE DAY COLUMNS TO TARGET PRODUCT
            targetProduct = pd.merge(targetProduct, otherAvailable, how='inner', left_on='calendar_date', right_index=True)

            # SELECTED NEEDED COLUMNS FOR REGRESSION
            columns = ['total_sales'] + keys
            targetColumns = targetProduct[columns]


            # BUILD REGRESSION FORMULA
            formula = buildFormula(keys)

            # CREATE RESULTS OBJECTS
            p_values = defaultdict(lambda: 0)
            coefficients = defaultdict(lambda: 0)
            sd = defaultdict(lambda:0)

            # MONTE CARLO CROSS VALIDATION
            numIterations = 10
            for i in xrange(numIterations):
                joinedTrain, joinedTest = train_test_split(targetColumns, test_size=.3, random_state=i)

                # BUILD MODEL FROM ORDINARY LEAST SQUARES REGRESSION
                model = smf.ols(formula=formula, data=joinedTrain).fit()
                coefficients['Intercept'] += model.params['Intercept']

                for k in keys:
                    p_values[k] += model.pvalues[k]
                    coefficients[k] += model.params[k]

            # AVERAGE VALUES
            for k in keys:
                p_values[k] /= float(numIterations)
                coefficients[k] /= float(numIterations)
                sd[k] = stdev(targetColumns[k])
            coefficients['Intercept'] /= float(numIterations)

            # FILTER OUT DATA WITH NO VARIATION IN AVAILABILITY
            if stdev(targetProduct["_" + str(product_key)]) < 0.1:
                continue

            # FILTER OUT DATA WITH HIGH P_VALUES
            if p_values["_" + str(product_key)] > 0.1:
                continue

            # FORMAT FOR GRAPH
            finalResultIndexes.append(store_id)
            changeInSales = coefficients["_" + str(product_key)] * -1.0
            finalResultValues.append(changeInSales)

        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            # OCCASIONALLY THERE IS NOT ENOUGH DATA AND YOU DIVIDE BY 0, WHICH THROWS AN ERROR
            continue

    finalResult[other_product_key] = pd.Series(finalResultValues, index=finalResultIndexes)

# PUT RESULTS INTO DATA FRAME
finalDF = pd.DataFrame(finalResult)

finalDF = finalDF.fillna(0)
finalMatrix = []
for key in otherProductsInCluster:
    finalMatrix.append(finalDF[key].values)

# USE CUSTOM GRAPHING MODULE
import graphy2
graphy2.showGraph(product_key, list(finalDF.index), otherProductsInCluster, finalMatrix)
