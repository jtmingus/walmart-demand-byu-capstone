# Walmart Demand BYU Capstone

## Included Files

#### clusters.csv
* Clusters of similar items
* Columns: product_key, cluster_id

#### train.csv
* Transaction data
* Columns: store_id, product_key, calendar_date, on_hand_qty, total_units, total_sales, promotional_units, promotional_sales, join_quality, available_day
* *Our program only uses store_id, product_key, calendar_date, total_sales, and available_day*

#### walmart_regression.py
* The main program

#### graphy2.py
* Custom module for handling our data and graphing it using `matplotlib`
