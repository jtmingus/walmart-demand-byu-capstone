import matplotlib.pyplot as plt
import numpy as np
import itertools
import math

def _showGraph(product_key, stores, products, coefficients, figure):
    # CONVERT DATA TO VALID MATPLOTLIB FORMAT
    zipped = zip(stores,coefficients[0])
    zipped.sort(key=lambda x:-x[1])
    zipped = zip(*zipped)
    stores = zipped[0]
    coefficients = [zipped[1]]
    ax = figure.add_subplot(111)
    data = []
    values = []
    for store in range(len(stores)):
        data.append({})
        for i in range(len(products)):
            if coefficients[i][store] != 0:
                data[-1][products[i]] = coefficients[i][store]
                values.append(coefficients[i][store])

    colors = ['#76c043', '#367c2b', '#78b9e7', '#007dc6', '#ffc220', '#f47321', '#004c91']
    inds = []
    c = 1
    for i in data:
        for d in sorted(i.keys()):
            inds.append(c)
            c += 1

    # DEFINE AXIS SIZES
    _ymin = 100
    _ymax = 0
    for i in coefficients:
        for j in i:
            if j<_ymin: _ymin = j
            if j>_ymax: _ymax = j
    ax.axis([0,c, _ymin-2 if _ymin < 0 else 0 ,_ymax+2])

    # DEFINE GROUP LABELS
    labels = ['']
    sid = 0
    for i in data:
        labels.append(str(stores[sid]))
        sid += 1

    # Creates discrete values for x co-ordinates (widths of the bars)
    x = np.array(inds)

    # Defines some random set of values for y (heights of the bars)
    y = np.array(values)

    # Replaces the names in the x-axis with labels
    ax.set_xticks(range(len(values)+1))
    ax.set_xticklabels(labels)

    # Creates the bar chart
    barlist = ax.bar(left = x, height=y)
    i = 0
    for store in data:
        for id in sorted(store.keys()):
            barlist[i].set_color(colors[products.index(id)])
            i += 1

    for i,v in enumerate(filter(lambda x:x!=0,coefficients[0])):
        ax.text(i + (.68 if v>=10 else + .75), v + (.5 if v>0 else -.8),  str(round(v,1)))
    ax.set_ylabel('Change in Sales')
    ax.set_xlabel('Stores')
    ax.set_title("Effect of Removing Product " + str(product_key) + " on Sales of Product " + str(products[0]) + " by Store")


def showGraph(product_key,stores,products,coefficients):
    for i in range(len(products)):
        _showGraph(product_key,stores,[products[i]],[[x for x in coefficients[i]]], plt.figure())
    plt.show()
